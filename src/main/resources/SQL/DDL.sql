CREATE DATABASE GroStu;

CREATE TABLE groups (
	id INT PRIMARY KEY,
	name VARCHAR(255)
);

CREATE TABLE student (
	id INT PRIMARY KEY,
    name VARCHAR(255),
    phone VARCHAR(255),
    groupId INT
);

