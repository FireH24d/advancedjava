INSERT INTO groups (id, name) VALUES
(1, 'IT'),
(2, 'CS'),
(3, 'ITM'),
(4,'SE'),
(5,'TS');

INSERT INTO student (id, name, phone, groupId) VALUES
(1, 'Ayanbek', '87752676931', 2),
(2, 'Aman', '87027145445', 3),
(3, 'Ernar', '87118615355', 5),
(4, 'Alikhan', '87585855884', 1),
(5, 'Ivan', '85698585748', 2),
(6, 'Ainur', '87753708514', 4),
(7, 'Ermek', '85837678415', 4),
(8, 'Talgat', '87936583463', 1),
(9, 'Aslan', '84167539032', 3),
(10, 'Rick', '83764467648', 5);

