import Repository.GroupRepository;
import Repository.StudentRepository;

public class Main {
    public static void main(String[] args)  {
        System.out.println("Printing Group");
        GroupRepository groupRepository = new GroupRepository();
        groupRepository.printGroups();

        System.out.println("Printing Students by Group");
        StudentRepository studentRepository = new StudentRepository();
        studentRepository.printStudents();
    }
}
