package Repository;


import Entity.Group;

import java.sql.*;

public class GroupRepository {
    private final String url = "jdbc:postgresql://localhost:5432/GroStu";
    private final String user = "postgres";
    private final String password = "serikov1!";

    Connection conn = null;
    Statement st = null;

    public void printGroups() {
        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected!");

            st = conn.createStatement();
            String sql = "SELECT * FROM groups";
            ResultSet rs = st.executeQuery(sql);

            while(rs.next()) {
                Group gr = new Group(rs.getInt(1), rs.getString(2));
                System.out.println(gr.toString());
            }
            rs.close();
        }
        catch (SQLException se) {
            se.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (st != null)
                    st.close();
            }
            catch (SQLException se) {
            }
            try {
                if (conn != null)
                    conn.close();
            }
            catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
