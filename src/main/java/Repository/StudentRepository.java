package Repository;

import Entity.Student;

import java.sql.*;

public class StudentRepository {
    private final String url = "jdbc:postgresql://localhost:5432/GroStu";
    private final String user = "postgres";
    private final String password = "serikov1!";

    Connection conn = null;
    Statement st = null;

    public void printStudents() {
        try {

            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected!");

            st = conn.createStatement();

            String sql = "SELECT * FROM student ORDER BY groupId";
            ResultSet rs = st.executeQuery(sql);


            while(rs.next()) {
                Student a = new Student(rs.getInt("id"), rs.getString("name"),
                        rs.getString("phone"), rs.getInt("groupId"));
                System.out.println(a.toString());
            }
            rs.close();
        }
        catch (SQLException se) {
            se.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        finally {
            try {
                if (st != null)
                    st.close();
            }
            catch(SQLException se){
            }

            try {
                if (conn != null)
                    conn.close();
            }
            catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}
