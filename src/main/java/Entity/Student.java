package Entity;

import lombok.Data;

@Data
public class Student {
    private int id;
    private String name;
    private String phone;
    private int groupId;

    public Student(int id, String name, String phone, int groupId) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.groupId = groupId;
    }
}
