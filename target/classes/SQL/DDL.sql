CREATE DATABASE GroStu;

CREATE TABLE Groups (
	id INT PRIMARY KEY,
	name VARCHAR(255)
);

CREATE TABLE Student (
	id INT PRIMARY KEY,
    name VARCHAR(255),
    phone VARCHAR(255),
    groupId INT
);

